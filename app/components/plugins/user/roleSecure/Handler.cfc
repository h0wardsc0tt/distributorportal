<cfcomponent extends="org.mangoblog.plugins.BasePlugin">
	<cfset variables.package = "com/asfusion/mango/plugins/authorsonly"/>
	
<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->	
	<cffunction name="init" access="public" output="true" returntype="any">
		<cfargument name="mainManager" type="any" required="true" />
		<cfargument name="preferences" type="any" required="true" />
		
			<cfset setManager(arguments.mainManager) />
			<cfset setPreferencesManager(arguments.preferences) />
			
		<cfreturn this/>
	</cffunction>


<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
	<cffunction name="processEvent" hint="Synchronous event handling" access="public" output="true" returntype="any">
		<cfargument name="event" type="any" required="true" />

			<cfset var data = arguments.event.data />
			<cfset var eventName = arguments.event.name />
			<cfset var manager = getManager() />
			<cfset var templates = "" />
			<cfset var template = "" />
			<cfset var loginContent = "" />
			<cfset var address = "" />
			<cfset var errormsg = "" />
			<cfset var thisItem = "" />
			<cfset var secure = structNew()>
			<cfset var currentRole = "">
						
			<cfif eventName contains "post">
				<cfset thisItem = manager.getPostsManager().getPostByName(data.externaldata.postname)>
			<cfelse>
				<cfset thisItem = manager.getPagesManager().getPageByName(data.externaldata.pagename)>
			</cfif>
			
			<cfset secure.isSecure = thisItem.customFieldExists('secure')> 

			<cfif secure.isSecure>
				<cfset secure.secureValue = thisItem.getCustomField('secure').value>
			<cfelse>
				<cfreturn arguments.event />
			</cfif>
			
			<cfif manager.ISCURRENTUSERLOGGEDIN()>
				<cfset currentRole = manager.GETCURRENTUSER().GETCURRENTROLE().getID()>				
			</cfif>
			
			<!---If User Does Not Have Access To page--->
			<cfif ! listFindNoCase(secure.secureValue, currentRole)>
				<cfif CGI.CF_TEMPLATE_PATH CONTAINS "page.cfm">
					<cfset template = "noaccess_page.cfm" />
				<cfelse>
					<cfset template = "noaccess.cfm" />
				</cfif>

				<!---2013/11/26 ATN: Removed lines 55-84. Pushing through extra logic unneeded using noaccess.cfm page for simplification
				<!---Begin Remove--->
				<!--- first check if this page has been defined in the current skin --->
				<cfset templates = manager.getAdministrator().getAdminPageTemplates()>
				<cfif structkeyexists(templates,"login")>
					<cfset template = templates['login'].file />
					<cfinclude template="login.cfm">
				<cfelse>
					<cfset address = cgi.script_name />
					<cfif len(cgi.path_info) AND cgi.path_info NEQ cgi.script_name>
						<cfset address = address & cgi.path_info />
					</cfif>
					<cfif len(cgi.query_string)>
						<cfset address = address & "?" & cgi.query_string />
					</cfif>
					<cfif structkeyexists(request, "errormsg")>
						<cfset errormsg = request.errormsg />
					</cfif>
					<cfsavecontent variable="loginContent">
						<cfif len(currentRole)>						
							<h1 style="font-size:36px;font-weight:bold">Your current security level does not allow you access to this area.</h1>
						<cfelse>
							<h1 style="font-size:36px;font-weight:bold">Please login.</h1>
							<cfinclude template="login.cfm">
						</cfif>
					</cfsavecontent>
					<cfif !len(currentRole)>	
						<cfset data.message.setTitle("Login") />
					</cfif>
					<cfset data.message.setText(loginContent) />
				</cfif>2013/11/26 ATN: End Remove--->
				
				<cfif eventName EQ "beforeIndexTemplate">
					<cfset data.indexTemplate = template />
				<cfelseif eventName EQ "beforePageTemplate">
					<cfset data.pageTemplate = template />
				<cfelseif eventName EQ "beforePostTemplate">
					<cfset data.postTemplate = template />
				<cfelseif eventName EQ "beforeArchivesTemplate">
					<cfset data.archivesTemplate = template />
				<cfelseif eventName EQ "beforeAuthorTemplate">
					<cfset data.authorTemplate = template />
				</cfif>
			</cfif>
		
		<cfreturn arguments.event />
	</cffunction>
	
</cfcomponent>