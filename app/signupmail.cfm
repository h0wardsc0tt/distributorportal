<cfimport prefix="mango" taglib="/tags/mango">
<cfimport prefix="mangox" taglib="/tags/mangoextras">
<cfimport prefix="template" taglib=".">
<cfsilent>
<cfscript>
	ERR = StructNew();
	ERR.ErrorFound = false;
	ERR.ErrorMessage = "";
	
	if(Len(FORM.companyName) EQ 0) {
		ERR.ErrorFound = true;
		ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Company name may not be blank. Please try again");
	} 
	if(Len(FORM.firstName) EQ 0) {
		ERR.ErrorFound = true;
		ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "First name may not be blank. Please try again");
	} 
	if(Len(FORM.lastName) EQ 0) {
		ERR.ErrorFound = true;
		ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Last name may not be blank. Please try again");
	}
	if(FORM.emailAdd IS NOT "") {
		//Validate proper email
		if(ReFindNoCase("^([a-zA-Z0-9_\-\.\+])+@(([0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5])|((([a-zA-Z0-9\-])+\.)+([a-zA-Z\-])+))$", FORM.emailAdd) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Invalid E-mail supplied. Please try again");
		}
	} else {
		ERR.ErrorFound = true;
		ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "E-mail may not be blank. Please try again");
	}
	if(Len(FORM.passwordB) EQ 0) {
		ERR.ErrorFound = true;
		ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password may not be blank. Please try again");
	}
</cfscript>
</cfsilent>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head">
	<meta http-equiv="Content-Type" content="text/html; charset=<mango:Blog charset />" />
	<title>Register &#8212; <mango:Blog title /></title>
	
	<meta name="generator" content="Mango <mango:Blog version />" />
	
	<link rel="stylesheet" href="<mango:Blog skinurl />assets/styles/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<mango:Blog skinurl />assets/styles/custom.css" type="text/css" media="screen" />
	<!--[if lte IE 7]>
	<link rel="stylesheet" type="text/css" href="<mango:Blog skinurl />assets/styles/ie7.css" media="screen" />
	<![endif]-->
	<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="<mango:Blog skinurl />assets/styles/ie6.css" media="screen" />
	<![endif]-->
	
	<meta name="robots" content="noindex, nofollow" />
	<mango:Event name="beforeHtmlHeadEnd" />
</head>
<body class="custom">
<mango:Event name="beforeHtmlBodyStart" />
<div id="container">
	<!--<div id="masthead">
		<h1><a href="<mango:Blog url />"><mango:Blog title /></a></h1>
		<h3><mango:Blog tagline /></h3>
	</div>-->

	<div id="hme-logo">
		<a href="/"><img src="/images/HME-logo-large.png" width="*" height="44" border="0" alt="HME logo large"/></a>
	</div>

	<div id="header_img">
		<a href="<mango:Blog basePath />"><img src="/images/International-Distributors.jpg" width="930" alt="<mango:Blog title />" title="<mango:Blog title />" /></a>
	</div>
	<div id="content_box">
	<cfif ERR.ErrorFound>
		<div id="content" class="pages">
			
			<h2>Register</h2>
			<div class="entry">
				<mango:RequestVar ifExists="errormsg">
					<p class="error"><mango:RequestVar name="errormsg" /></p>
				</mango:RequestVar>
			<cfoutput><cfloop list="#ERR.ErrorMessage#" index="thisError"><li style="color:Red;">#thisError#</li></cfloop></cfoutput>
			<cfoutput><form action="signupmail.cfm" method="POST" id="login_form"></cfoutput>
					<p>
					<label for="companyName">Company Name:</label>
					<input name="companyName" id="companyName" value="<cfoutput>#FORM.companyName#</cfoutput>" size="22" type="text" class="text_input">
					</p>
					<p>
					<label for="firstName">First Name:</label>
					<input name="firstName" id="firstName" value="<cfoutput>#FORM.firstName#</cfoutput>" size="22" type="text" class="text_input">
					</p>
					<p>
					<label for="lastName">Last Name:</label>
					<input name="lastName" id="lastName" value="<cfoutput>#FORM.lastName#</cfoutput>" size="22" type="text" class="text_input">
					</p>
					<p>
					<label for="emailAdd">Email Address:</label>
					<input name="emailAdd" id="emailAdd" value="<cfoutput>#FORM.emailAdd#</cfoutput>" size="22" type="text" class="text_input">
					</p>
					<p>
					<label for="password">Password:</label><br />
					<input name="passwordB" id="password" value="<cfoutput>#FORM.passwordB#</cfoutput>" size="22" type="password" class="text_input">
					</p>
					<input name="register" class="form_submit" type="submit" id="submit" src="<mango:Blog skinurl />assets/images/submit_comment.gif" value="Register" />
				</form>
			</div>
			<div class="clear"></div>
	
			</div>
			</div>

	<div id="footer"><mango:Event name="afterFooterStart" />
		<p>&#169; <cfoutput>#Year(Now())#</cfoutput> HME Wireless, Inc.</p>
	<mango:Event name="beforeFooterEnd" />
	</div>
</div>
<mango:Event name="beforeHtmlBodyEnd" />
</body>
</html>
	<cfelse>
		<div id="content" class="pages">
			<h2>Thank You</h2>
			<p style="font-size:1.2em;">Your Registration has been Submitted.</p>
			<p style="font-size:1.2em;margin-bottom:20px;">We will contact you as soon as your information has been verified.</p>
			<cfmail 
				from="no-reply@hme.com" 
				to="sstedd@hme.com" 
				subject="New Portal Login Request">
The following user has requested a login:

Company:  #FORM.companyName#
First Name:  #FORM.firstName#
Last Name:  #FORM.lastName#
Email:  #FORM.emailAdd#
Password:  #FORM.passwordB#

http://distributorportal.hme.com/
			</cfmail>
		</div>
	</div>
	<div id="footer"><mango:Event name="afterFooterStart" />
		<p>&#169; <cfoutput>#Year(Now())#</cfoutput> HME Wireless, Inc.</p>
	<mango:Event name="beforeFooterEnd" />
	</div>
</div>
<mango:Event name="beforeHtmlBodyEnd" />
</body>
</html>

<meta http-equiv="refresh" content="5;url=index.cfm" />
</cfif>