<cfimport prefix="mango" taglib="../../tags/mango">
<cfimport prefix="mangox" taglib="../../tags/mangoextras">
<cfimport prefix="template" taglib=".">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<mango:Blog charset />" />
	<title><mango:Blog title /> &#8212; <mango:Blog tagline /></title>
	<meta name="generator" content="Mango <mango:Blog version />" />
	<meta name="description" content="<mango:Blog description />" />
	<link rel="stylesheet" href="<mango:Blog skinurl />assets/styles/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<mango:Blog skinurl />assets/styles/custom.css" type="text/css" media="screen" />
	<!--[if lte IE 7]>
	<link rel="stylesheet" type="text/css" href="<mango:Blog skinurl />assets/styles/ie7.css" media="screen" />
	<![endif]-->
	<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="<mango:Blog skinurl />assets/styles/ie6.css" media="screen" />
	<![endif]-->
	<meta name="robots" content="index, follow" />
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<mango:Blog rssurl />" />
	<link rel="alternate" type="application/atom+xml" title="Atom" href="<mango:Blog atomurl />" />	
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="<mango:Blog apiurl />" />
	<mango:Event name="beforeHtmlHeadEnd" />
</head>

<body class="custom" style="margin-top:10px;">
<div id="page-container">
	<div id="hme-logo">
		<a href="/"><img src="/images/HME-logo-large.png" width="*" height="44" border="0" alt="HME logo large"/></a>
	</div>
		<style type="text/css" media="all">
		@import "/skins/cutline/colorbox.css";
		</style>
		<cfinclude template="/cfincludes/jquery.cfm">
		<script type="text/javascript" src="/skins/cutline/jquery.colorbox.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){								   
				//Examples of how to assign the ColorBox event to elements
				$(".productgallery").colorbox({width:"900px", height:"700px", iframe:true});
				$(".example8").colorbox({width:"950px", height:"600px", inline:true, href:"#inline_example1"});
				$(".hmew-video").colorbox({iframe:true, innerWidth:630, innerHeight:380});
				$("a[target='hmew-video']").colorbox({rel:"group1", iframe:true, innerWidth:630, innerHeight:380});
		
			});
		</script>
		<ul id="nav">
			<li><a class="current" href="<mango:Blog basePath />">Distributor Portal Home</a></li>
			<li><a class="current" href="http://www.hme.com">return to website</a></li>
			<!---<li class="rss"><a href="<mango:Blog rssurl />">RSS</a></li>--->
			<li><a href="<mango:Blog basePath />./admin/">Admin</a></li>
			<li><a href="<mango:Blog basePath />./index.cfm?logout=1">Logout</a></li>
		</ul>
	</div>
	<mango:Event name="beforeHtmlBodyStart" />
	<div id="container">
		<div id="header_img">
			<a href="<mango:Blog basePath />"><img src="/images/International-Distributors.jpg" width="930" alt="<mango:Blog title />" title="<mango:Blog title />" /></a>
		</div>
		<div id="content_box">
		
			<div id="content" class="posts">
			<div class="intro">
			<h1>Welcome to the HME International Distributor Portal</h1>
			<p>We understand that as our international distributor you need the latest, most up-to-date information to help you succeed. This portal was developed to do just that! Here you will find all technical, sales, and marketing documentation safely stored in one convenient place. Connect to other HME portals, videos, images, and more with just a click. Subscribe to the portal's blog and get new developments delivered straight to your email as they are posted to the site. Have comments, questions, or suggestions for us? Post them to this portal to help us improve our support. We are here to help you optimize your QSR profits, so look around and take advantage of all that this new tool has to offer!</p>
			<p style="font-size:0.9em;margin-top:5px;">*Reminder: The information contained on this portal is confidential. The use of any information from this portal is covered under the HME International Distributor Agreement and its accompanying Nondisclosure Agreement.</p></div>
			<h3>Latest News</h3>
			<mango:Posts count="10">
				<mango:Post>	
					<h2><a href="<mango:PostProperty link />" rel="bookmark" title="Permanent Link to <mango:PostProperty title />"><mango:PostProperty title /></a></h2>
					<h4><mango:PostProperty date dateformat="mmmm dd, yyyy" /> &middot; By <mango:PostProperty author /> &middot; <mango:PostProperty ifcommentsallowed><a href="<mango:PostProperty link />#respond" title="Comment on <mango:PostProperty title />"><mango:PostProperty ifCommentCountGT="0"><mango:PostProperty commentCount /> Comment<mango:PostProperty ifCommentCountGT="1">s</mango:PostProperty></mango:PostProperty><mango:PostProperty ifCommentCountLT="1">No Comments</mango:PostProperty></a></mango:PostProperty></h4>
					<div class="entry">
						<mango:PostProperty ifhasExcerpt excerpt>
							<p><a href="<mango:PostProperty link />" title="Read the rest of this entry">[Read more &rarr;]</a></p>
						</mango:PostProperty>
						<mango:PostProperty ifNotHasExcerpt body />
					</div>
					<div class="entry-footer entry">
						<mango:Event name="beforePostContentEnd" template="index" mode="excerpt" />
					</div>
					<p class="tagged">
						<span class="add_comment">
							<mango:PostProperty ifcommentsallowed>&rarr; <a href="<mango:PostProperty link />#respond" title="Comment on <mango:PostProperty title />"><mango:PostProperty ifCommentCountGT="0"><mango:PostProperty commentCount /> Comment<mango:PostProperty ifCommentCountGT="1">s</mango:PostProperty></mango:PostProperty><mango:PostProperty ifCommentCountLT="1">No Comments</mango:PostProperty></a></mango:PostProperty>
						</span>
						<strong>Tags:</strong> 
						<mango:Categories><mango:Category><a href="<mango:CategoryProperty link />" title="View all posts in  <mango:CategoryProperty title />" rel="category tag"><mango:CategoryProperty title /></a>
							<mango:Category ifCurrentIsNotLast>&middot; </mango:Category></mango:Category>
						</mango:Categories>
					</p>
					<div class="clear"></div>
				</mango:Post>
			</mango:Posts>
				
			<mango:Archive pageSize="10">
			<div class="navigation">
				<div class="previous"><mango:ArchiveProperty ifHasNextPage><a class="previous" href="<mango:ArchiveProperty link pageDifference="1" />">&larr; Previous Entries</a></mango:ArchiveProperty></div>
				<div class="next"></div>
			</div>
			</mango:Archive>
			<div class="clear flat"></div>
		</div>
	<div id="sidebar">
		<ul class="sidebar_list">
			<mangox:PodGroup locationId="sidebar" template="index">
				<mangox:TemplatePod id="blog-description" title="HM Electronics, Inc.">
				<p><mango:Blog description descriptionParagraphFormat /></p>
				</mangox:TemplatePod>
				<template:sidebar />
			</mangox:PodGroup>	
		</ul>
	</div>	
	</div>
	<div id="footer">
		<mango:Event name="afterFooterStart" />
			<p>&#169; <cfoutput>#Year(Now())#</cfoutput> HM Electronics, Inc.</p>
		<mango:Event name="beforeFooterEnd" />
	</div>
</div>
<mango:Event name="beforeHtmlBodyEnd" />
</body>
</html>
<br clear="all" />