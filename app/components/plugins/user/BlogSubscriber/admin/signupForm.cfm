<cfoutput>
	
<cfif isDefined("form.subscribeEmail")>
	<script type="text/javascript">
		<cfif variables.blogSubscriberConfirm>
			alert("An email is being sent to confirm your subscription to this site.");
		<cfelse>
			alert("You have successfully subscribed to this site!");
		</cfif>
	</script>
</cfif>

<cfform method="post" action="#cgi.script_name#" name="subscribeForm">
	<fieldset>
		<legend>Subscribe to Blog</legend>
		<p><cfinput type="text" name="subscribeEmail" size="20" required="true" validate="email" validateat="onSubmit,onServer" message="Valid email address required" /></p>
	</fieldset>
	<div class="actions">
	    <input type="submit" class="primaryAction" value="Submit"/>
		<input type="hidden" name="action" value="event" />
		<input type="hidden" name="event" value="blogSubscribe" />
		<input type="hidden" name="apply" value="true" />
		<input type="hidden" name="selected" value="BlogSubscriber" />
	</div>
</cfform>

</cfoutput>