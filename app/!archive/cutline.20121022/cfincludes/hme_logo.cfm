<div id="hme-logo"><a href="/"><img src="/images/HMEW-logo-large.png" width="96" height="56" border="0" alt="HME logo large"/></a></div>
<style type="text/css" media="all">
@import "/skins/cutline/colorbox.css";
</style>
<cfinclude template="/cfincludes/jquery.cfm">
<script type="text/javascript" src="/skins/cutline/jquery.colorbox.js"></script>
        <script type="text/javascript">
			$(document).ready(function(){								   
				//Examples of how to assign the ColorBox event to elements
				$(".productgallery").colorbox({width:"900px", height:"700px", iframe:true});
				$(".example8").colorbox({width:"950px", height:"600px", inline:true, href:"#inline_example1"});
				$(".hmew-video").colorbox({iframe:true, innerWidth:630, innerHeight:380});
				$("a[target='hmew-video']").colorbox({rel:"group1", iframe:true, innerWidth:630, innerHeight:380});

			});
		</script>